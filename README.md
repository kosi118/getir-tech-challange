# Getir-Tech-Challange

This repo includes Golang application for Getir Tech Company Challange

## Getting started
You should have installed go 1.15 above and for testing it will be helpful if you have a locally ready running mongodb server

```
cd existing_repo
git remote add origin https://gitlab.com/kosi118/getir-tech-challange.git
git branch -M main
git push -uf origin main
```

## Test and Deploy
```
go test ./test/...
```

You must install golang and set PATH variable correctly, then install project requirements
```
go mod vendor
```
Then build application
```
go build -o getirServer
```
It will build a executable binary file named "getirServer"
***
You must have a config file like "config.json", you can copy from "config.json.example"
```
cp config.json.example config.json
```
Modify config.json file with your real configuration
***

Then you can run it like
```
./getirServer
```
Or Specify config file
```
./getirServer --config=config.json
```
***

## Authors and acknowledgment
Osman Çevik (kosi118@gmail.com)

## License
Apache License
