package controller

import (
	"encoding/json"
	"getir-tech-challange/cmd/server/handler"
	"getir-tech-challange/cmd/server/request"
	"getir-tech-challange/cmd/server/service"
	InMemory2 "getir-tech-challange/util/InMemory"
	"net/http"
	"strings"
)

type InMemory struct {
}

/**
Getting values by key from In Memory Database
*/
func (inMemory *InMemory) Get(w http.ResponseWriter, r *http.Request) {
	var requestObj request.InMemoryRequest
	response := make(map[string]interface{})

	keyParam := r.URL.Query().Get("key")
	requestObj.Key = &keyParam

	validator := request.InMemory{Request: requestObj}
	isValid, errs := validator.Validate()
	if !isValid {
		var errsStr []string
		for _, errObj := range errs {
			errsStr = append(errsStr, errObj.Error())
		}
		http.Error(w, strings.Join(errsStr, ", "), http.StatusBadRequest)
		return
	}
	inMemoryService := service.NewInMemoryService(InMemory2.GetInMemoryInstance())
	value, getErr := inMemoryService.Get(*requestObj.Key)

	if getErr != nil {
		http.Error(w, getErr.Error(), http.StatusBadRequest)
		return
	}
	response["key"] = *requestObj.Key
	response["value"] = value

	js, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

/**
Setting values with key to In Memory Database
*/
func (inMemory *InMemory) Set(w http.ResponseWriter, r *http.Request) {
	var requestObj request.InMemoryRequest

	err := json.NewDecoder(r.Body).Decode(&requestObj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	validator := request.InMemory{Request: requestObj}
	isValid, errs := validator.Validate()
	if !isValid {
		var errsStr []string
		for _, errObj := range errs {
			errsStr = append(errsStr, errObj.Error())
		}
		http.Error(w, strings.Join(errsStr, ", "), http.StatusBadRequest)
		return
	}

	inMemoryService := service.NewInMemoryService(InMemory2.GetInMemoryInstance())
	inMemoryService.Set(*requestObj.Key, *requestObj.Value)

	js, err := json.Marshal(requestObj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

/**
Resolve process to handle Post and Get Requests
*/
func (inMemory *InMemory) Resolve(writer http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case "POST":
		inMemory.Set(writer, request)
	case "GET":
		inMemory.Get(writer, request)
	default:
		handler.NotAllowed(writer)
	}
}
