package controller

import (
	"encoding/json"
	"getir-tech-challange/cmd/server/handler"
	"getir-tech-challange/cmd/server/request"
	"getir-tech-challange/cmd/server/service"
	"getir-tech-challange/model"
	"getir-tech-challange/util/database"
	"net/http"
)

type Record struct {
}

/**
Getting records with filter parameters from MongoDb Database
*/
func (record *Record) GetRecords(w http.ResponseWriter, r *http.Request) {
	var requestObj request.RecordRequest
	response := make(map[string]interface{})

	err := json.NewDecoder(r.Body).Decode(&requestObj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	validator := request.Record{Request: requestObj}
	isValid, errs := validator.Validate()
	if !isValid {
		var errsStr []string
		for _, errObj := range errs {
			errsStr = append(errsStr, errObj.Error())
		}
		response["code"] = 1
		response["msg"] = "Failed"
		response["errors"] = errsStr
	} else {
		manager := database.DbManager{}
		recordService := service.NewRecordService(manager.GetDatabaseInstance())
		filter := model.NewFilterByRequest(requestObj)
		records := recordService.GetWithFilter(*filter)
		response["code"] = 0
		response["msg"] = "Success"
		response["records"] = records
	}

	js, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if response["code"] != 0 {
		w.WriteHeader(http.StatusBadRequest)
	}

	w.Write(js)

}

/**
Resolve process to handle Only Post Requests, If not returns NotAllowed
*/
func (record *Record) Resolve(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		record.GetRecords(w, r)
	default:
		handler.NotAllowed(w)
	}
}
