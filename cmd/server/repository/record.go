package repository

import (
	"context"
	"getir-tech-challange/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

type RecordRepositoryI interface {
	GetWithFilter(filter model.Filter) []model.Record
}

type RecordRepository struct {
	Collection *mongo.Collection
}

/**
Filters records with filter object, uses aggregation, projection methods to getting records
*/
func (recordRepository RecordRepository) GetWithFilter(filter model.Filter) []model.Record {
	var records = make([]model.Record, 0)
	filterObj := recordRepository.GetFilterObject(filter)
	pipeline := bson.A{
		bson.D{{
			"$project",
			bson.D{
				{"key", 1},
				{"createdAt", 1},
				{"totalCount", bson.D{
					{"$sum", "$counts"},
				}},
			},
		}},
		bson.D{
			{"$match", filterObj},
		},
	}
	cursor, err := recordRepository.Collection.Aggregate(context.Background(), pipeline)
	if err != nil {
		log.Fatal(err)
	}

	if err = cursor.All(context.Background(), &records); err != nil {
		log.Fatal(err)
	}

	return records
}

/**
Building Filter Object with request parameters
*/
func (recordRepository RecordRepository) GetFilterObject(filter model.Filter) bson.M {
	filterObj := bson.M{}
	if len(filter.EndDate) > 0 || len(filter.StartDate) > 0 {
		dateFilterObj := bson.M{}
		var err error
		if len(filter.EndDate) > 0 {
			dateFilterObj["$lte"], err = time.Parse("2006-01-02", filter.EndDate)
			if err != nil {
				log.Println("parsing date problem")
				delete(dateFilterObj, "$lt")
			}
		}
		if len(filter.StartDate) > 0 {
			dateFilterObj["$gte"], err = time.Parse("2006-01-02", filter.StartDate)
			if err != nil {
				log.Println("parsing date problem")
				delete(dateFilterObj, "$gt")
			}
		}
		filterObj["createdAt"] = dateFilterObj
	}

	if filter.MinCount > 0 || filter.MaxCount > 0 {
		countFilterObj := bson.M{}
		if filter.MaxCount > 0 {
			countFilterObj["$lte"] = filter.MaxCount
		}
		if filter.MinCount > 0 {
			countFilterObj["$gte"] = filter.MinCount
		}
		filterObj["totalCount"] = countFilterObj
	}

	return filterObj
}
