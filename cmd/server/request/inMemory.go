package request

import "errors"

type InMemory struct {
	Request InMemoryRequest
}

type InMemoryRequest struct {
	Key   *string `json: "key,omitempty"`
	Value *string `json: "value,omitempty"`
}

/**
Validates Request, If not returns errors
*/
func (inMemory *InMemory) Validate() (bool, []error) {
	var errorsArr []error
	if inMemory.Request.Key == nil || len(*inMemory.Request.Key) < 1 {
		errorsArr = append(errorsArr, errors.New("key parameter must be filled"))
	}
	return len(errorsArr) < 1, errorsArr
}
