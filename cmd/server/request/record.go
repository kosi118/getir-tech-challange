package request

import (
	"errors"
	"time"
)

type Record struct {
	Request RecordRequest
}

type RecordRequest struct {
	StartDate *string `json: "startDate,omitempty"`
	EndDate   *string `json: "endDate,omitempty"`
	MinCount  *int    `json: "minCount,omitempty"`
	MaxCount  *int    `json: "maxCount,omitempty"`
}

/**
Validates Record Request, incldes some conditions like MaxCount > MinCount or StartDate < EndDate
*/
func (record *Record) Validate() (bool, []error) {
	var errorsArr []error

	if (record.Request.MinCount != nil && record.Request.MaxCount != nil) && *record.Request.MinCount > *record.Request.MaxCount {
		errorsArr = append(errorsArr, errors.New("MaxCount must be grater than MinCount"))
	}

	if record.Request.StartDate != nil {
		_, validErr := time.Parse("2006-01-02", *record.Request.StartDate)
		if validErr != nil {
			errorsArr = append(errorsArr, errors.New("StartDate field must be valid date format -> YYYY-MM-DD"))
		}
	}
	if record.Request.EndDate != nil {
		_, validErr := time.Parse("2006-01-02", *record.Request.EndDate)
		if validErr != nil {
			errorsArr = append(errorsArr, errors.New("EndDate field must be valid date format -> YYYY-MM-DD"))
		}
	}

	if record.Request.StartDate != nil && record.Request.EndDate != nil {
		startDateParsed, validErr := time.Parse("2006-01-02", *record.Request.StartDate)
		endDateParsed, validErr2 := time.Parse("2006-01-02", *record.Request.EndDate)
		if validErr == nil && validErr2 == nil && !startDateParsed.Before(endDateParsed) {
			errorsArr = append(errorsArr, errors.New("EndDate must be grater than StartDate"))
		}
	}
	return len(errorsArr) < 1, errorsArr
}
