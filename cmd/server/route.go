package server

import (
	"getir-tech-challange/cmd/server/controller"
	"getir-tech-challange/cmd/server/handler"
	"net/http"
)

/**
Adding route handle definitions to http package
*/
func initRoutes() {
	recordContr := controller.Record{}
	inMemoryContr := controller.InMemory{}
	http.HandleFunc("/", handler.Index)
	http.HandleFunc("/records", recordContr.Resolve)
	http.HandleFunc("/in-memory", inMemoryContr.Resolve)
}
