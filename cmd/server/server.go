package server

import (
	"context"
	"getir-tech-challange/util/config"
	"getir-tech-challange/util/database"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type Server struct {
}

/**
Runs Http Server In Goroutine. Waits for terminate signals to gracefully shutdown http server
*/
func (serverObj *Server) Run() {
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	serverConf := config.GetConfigInstance().Server
	serverAddr := ":" + serverConf.Port
	srv := &http.Server{
		Addr:    serverAddr,
		Handler: nil,
	}

	initRoutes()
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	log.Print("Server Started")

	<-done
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer func() {
		// MongoDb connection should be closed
		manager := database.DbManager{}
		manager.GetDatabaseInstance().CloseConnection()
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Print("Server Exited Properly")
}
