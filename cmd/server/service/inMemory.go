package service

import (
	"getir-tech-challange/util/InMemory"
)

type InMemoryServiceI interface {
	Get(key string) (string, error)
	Set(key string, value string)
}

type InMemoryService struct {
	IManager InMemory.InMemoryManagerI
}

func NewInMemoryService(iManager InMemory.InMemoryManagerI) InMemoryServiceI {
	return &InMemoryService{IManager: iManager}
}

/**
Getting In memory Values, If not returns not found error
*/
func (inMemoryService *InMemoryService) Get(key string) (string, error) {
	response, err := inMemoryService.IManager.GetSafe(key)

	if err != nil {
		return "", err
	}
	return response, nil
}

/**
Setting In memory Values
*/
func (inMemoryService *InMemoryService) Set(key string, value string) {
	inMemoryService.IManager.SetSafe(key, value)
}
