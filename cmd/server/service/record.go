package service

import (
	"getir-tech-challange/cmd/server/repository"
	"getir-tech-challange/model"
	"getir-tech-challange/util/config"
	"getir-tech-challange/util/database"
)

type RecordServiceI interface {
	GetWithFilter(filter model.Filter) []model.Record
	GetCollectionName() string
}
type RecordService struct {
	MongoClient database.MongoClientI
}

func NewRecordService(mongoClient database.MongoClientI) RecordServiceI {
	return &RecordService{MongoClient: mongoClient}
}

func (recordService *RecordService) GetCollectionName() string {
	return "records"
}

/**
Building MongoDb Repository and uses to get records with filter
*/
func (recordService *RecordService) GetWithFilter(filter model.Filter) []model.Record {
	mongoDbConfig := config.GetConfigInstance().Database.Mongodb
	mongoDatabase := recordService.MongoClient.GetClient().Database(mongoDbConfig.DatabaseName)
	mongoCollection := mongoDatabase.Collection(recordService.GetCollectionName())
	repo := repository.RecordRepository{Collection: mongoCollection}
	return repo.GetWithFilter(filter)
}
