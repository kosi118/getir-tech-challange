module getir-tech-challange

go 1.15

require (
	github.com/magiconair/properties v1.8.5
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.7.1
)
