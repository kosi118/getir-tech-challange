package model

/**
This model represents config.json or any config files
*/

type Config struct {
	Database Database `json:"database"`
	Server   Server   `json:"server"`
}

type Database struct {
	Mongodb Mongodb `json:"mongodb"`
}

type Mongodb struct {
	Uri          string `json:"uri"`
	Hostname     string `json:"hostname"`
	Port         string `json:"port"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	DatabaseName string `json:"databaseName"`
	Options      string `json:"options"`
	Timeout      int64  `json:"timeout"`
}

type Server struct {
	Port string `json:"port"`
}
