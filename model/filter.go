package model

import "getir-tech-challange/cmd/server/request"

/**
Represents Filter Parameters To Get Records
*/

type Filter struct {
	StartDate string
	EndDate   string
	MinCount  int
	MaxCount  int
}

func NewFilter(startDate string, endDate string, minCount int, maxCount int) *Filter {
	return &Filter{
		StartDate: startDate,
		EndDate:   endDate,
		MinCount:  minCount,
		MaxCount:  maxCount,
	}
}

func NewFilterByRequest(request request.RecordRequest) *Filter {
	var filter = Filter{}
	if request.StartDate != nil {
		filter.StartDate = *request.StartDate
	}
	if request.EndDate != nil {
		filter.EndDate = *request.EndDate
	}
	if request.MinCount != nil {
		filter.MinCount = *request.MinCount
	}
	if request.MaxCount != nil {
		filter.MaxCount = *request.MaxCount
	}

	return &filter
}
