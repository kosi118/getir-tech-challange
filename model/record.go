package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

/**
Represents returned records with json template, also bson (Mongodb) definitions
*/

type Record struct {
	ID         primitive.ObjectID `json:"-" bson:"_id"`
	Key        string             `json:"key" bson:"key,omitempty"`
	CreatedAt  time.Time          `json:"createdAt" bson:"createdAt,omitempty"`
	TotalCount int                `json:"totalCount" bson:"totalCount,omitempty"`
}
