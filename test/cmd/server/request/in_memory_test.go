package request

import (
	"getir-tech-challange/cmd/server/request"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestValidate(t *testing.T) {
	key := "test1"
	value := "test2"
	requestObj := request.InMemory{Request: request.InMemoryRequest{
		Key:   &key,
		Value: &value,
	}}
	ok, errs := requestObj.Validate()
	assert.Empty(t, errs)
	assert.Equal(t, ok, true)

	requestObj = request.InMemory{Request: request.InMemoryRequest{
		Key:   nil,
		Value: nil,
	}}
	ok, errs = requestObj.Validate()
	assert.NotEmpty(t, errs)
	assert.Equal(t, ok, false)
	assert.Equal(t, "key parameter must be filled", errs[0].Error())
}
