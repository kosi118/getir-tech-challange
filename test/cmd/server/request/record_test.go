package request

import (
	"getir-tech-challange/cmd/server/request"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRecordValidate(t *testing.T) {
	requestObj := request.Record{Request: request.RecordRequest{
		StartDate: nil,
		EndDate:   nil,
		MinCount:  nil,
		MaxCount:  nil,
	}}
	ok, errs := requestObj.Validate()
	assert.Empty(t, errs)
	assert.Equal(t, ok, true)

	startDate := "2021-01-02"
	endDate := "2021-01-01"
	requestObj = request.Record{Request: request.RecordRequest{
		StartDate: &startDate,
		EndDate:   &endDate,
		MinCount:  nil,
		MaxCount:  nil,
	}}
	ok, errs = requestObj.Validate()
	assert.NotEmpty(t, errs)
	assert.Equal(t, "EndDate must be grater than StartDate", errs[0].Error())

	minCount := 100
	maxCount := 99
	requestObj = request.Record{Request: request.RecordRequest{
		StartDate: nil,
		EndDate:   nil,
		MinCount:  &minCount,
		MaxCount:  &maxCount,
	}}
	ok, errs = requestObj.Validate()
	assert.NotEmpty(t, errs)
	assert.Equal(t, "MaxCount must be grater than MinCount", errs[0].Error())

}
