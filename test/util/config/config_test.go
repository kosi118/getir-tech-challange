package config

import (
	"getir-tech-challange/model"
	"getir-tech-challange/util/config"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSetConfig(t *testing.T) {
	expectedObj := model.Config{
		Database: model.Database{
			Mongodb: model.Mongodb{
				Uri:          "mongodb://root:root@localhost:27017/db?authSource=admin",
				Hostname:     "",
				Port:         "",
				Username:     "",
				Password:     "",
				DatabaseName: "db",
				Timeout:      50,
			},
		},
		Server: model.Server{
			Port: "9001",
		},
	}
	config.SetConfig(&expectedObj)
	actualObj := config.GetConfigInstance()
	assert.Equal(t, expectedObj.Database.Mongodb.Timeout, actualObj.Database.Mongodb.Timeout)
	assert.Equal(t, expectedObj.Database.Mongodb.Uri, actualObj.Database.Mongodb.Uri)

}
