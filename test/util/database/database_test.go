package database

import (
	"context"
	"getir-tech-challange/model"
	"getir-tech-challange/util/config"
	"getir-tech-challange/util/database"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetDatabaseInstance(t *testing.T) {
	configObj := model.Config{
		Database: model.Database{
			Mongodb: model.Mongodb{
				Uri:          "mongodb://localhost:27017",
				Hostname:     "",
				Port:         "",
				Username:     "",
				Password:     "",
				DatabaseName: "",
				Timeout:      10,
			},
		},
	}
	config.SetConfig(&configObj)
	dbManager := database.DbManager{}
	dbInstance := dbManager.GetDatabaseInstance()
	err := dbInstance.GetClient().Ping(context.Background(), nil)
	assert.Equal(t, err, nil)

}

func TestGetUri(t *testing.T) {
	configObj := model.Config{
		Database: model.Database{
			Mongodb: model.Mongodb{
				Uri:          "mongodb://localhost:27017",
				Hostname:     "",
				Port:         "",
				Username:     "",
				Password:     "",
				DatabaseName: "",
				Timeout:      10,
			},
		},
	}
	config.SetConfig(&configObj)

	uri := database.GetUri()
	assert.Equal(t, uri, configObj.Database.Mongodb.Uri)

	configObj = model.Config{
		Database: model.Database{
			Mongodb: model.Mongodb{
				Uri:          "",
				Hostname:     "localhost",
				Port:         "27017",
				Username:     "",
				Password:     "",
				DatabaseName: "",
				Timeout:      10,
			},
		},
	}
	config.SetConfig(&configObj)
	uri = database.GetUri()
	assert.Equal(t, uri, "mongodb://localhost:27017")
}
