package database

import (
	"getir-tech-challange/util/InMemory"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInMemorySetGet(t *testing.T) {

	inMemoryInstance := InMemory.GetInMemoryInstance()
	key := "custom_key"
	value := "custom_value"
	inMemoryInstance.SetSafe(key, value)
	val, err := inMemoryInstance.GetSafe(key)
	assert.Equal(t, err, nil)
	assert.Equal(t, val, value)

	inMemoryInstance.SetSafe(key, "asdqweasd")
	val, err = inMemoryInstance.GetSafe(key)
	assert.NotEqual(t, val, value)

	val, err = inMemoryInstance.GetSafe("notFound")
	assert.NotEqual(t, err, "key does not exists")
	assert.Equal(t, val, "")
}
