package InMemory

import (
	"errors"
	"sync"
)

var once sync.Once

var inMemoryInstance *InMemoryManager

type InMemoryManagerI interface {
	GetSafe(key string) (string, error)
	SetSafe(key string, value string)
}

type InMemoryManager struct {
	database  map[string]string
	writeLock sync.RWMutex
}

func GetInMemoryInstance() InMemoryManagerI {
	once.Do(func() {
		inMemoryInstance = &InMemoryManager{
			database:  map[string]string{},
			writeLock: sync.RWMutex{},
		}
	})
	return inMemoryInstance
}

func NewInMemoryManager() InMemoryManagerI {
	return &InMemoryManager{
		database:  map[string]string{},
		writeLock: sync.RWMutex{},
	}
}

func (i *InMemoryManager) SetSafe(key string, value string) {
	i.writeLock.Lock()
	defer i.writeLock.Unlock()
	i.database[key] = value
}

func (i *InMemoryManager) GetSafe(key string) (string, error) {
	i.writeLock.RLock()
	defer i.writeLock.RUnlock()
	if _, ok := i.database[key]; !ok {
		return "", errors.New("key does not exists")
	}
	return i.database[key], nil
}
