package config

import "getir-tech-challange/model"

var configObj = &model.Config{}

func SetConfig(config *model.Config) {
	configObj = config
}

func GetConfigInstance() *model.Config {
	return configObj
}
