package database

import (
	"context"
	"fmt"
	"getir-tech-challange/util/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"sync"
	"time"
)

type DbService interface {
	GetDatabaseInstance() MongoClientI
	setDatabase(mongoClient MongoClientI)
	load() MongoClientI
	getDatabase() MongoClientI
}

type DbManager struct {
}

type MongoClientI interface {
	GetClient() *mongo.Client
	CloseConnection()
}

type MongoClient struct {
	Client *mongo.Client
}

func (mongoClient *MongoClient) GetClient() *mongo.Client {
	return mongoClient.Client
}

func (mongoClient *MongoClient) SetClient(client *mongo.Client) {
	mongoClient.Client = client
}

func (mongoClient *MongoClient) CloseConnection() {
	err := mongoClient.Client.Disconnect(context.Background())
	if err != nil {
		mongoClient.Client = nil
	} else {
		fmt.Println("Mongodb connection is closed")
	}
}

var once sync.Once

var databaseInstance MongoClient

func (manager *DbManager) GetDatabaseInstance() MongoClientI {
	once.Do(func() {
		manager.setDatabase(manager.load())
	})

	return manager.getDatabase()
}

func (manager *DbManager) setDatabase(mongoClient MongoClientI) {
	databaseInstance.SetClient(mongoClient.GetClient())
}

func (manager *DbManager) load() MongoClientI {
	mongoClient := MongoClient{}
	uri := GetUri()
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))

	if err != nil {
		log.Fatalln(err.Error())
	}
	var timeoutSec = config.GetConfigInstance().Database.Mongodb.Timeout

	ctx, _ := context.WithTimeout(context.Background(), time.Second*time.Duration(timeoutSec))
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	mongoClient.SetClient(client)
	return &mongoClient
}

func GetUri() string {
	// Example -> mongodb+srv://user:password@domain.com:27017/databaseName?retryWrites=true
	configInstance := config.GetConfigInstance()
	mongodbConfig := configInstance.Database.Mongodb
	if len(mongodbConfig.Uri) > 0 {
		return mongodbConfig.Uri
	}
	var authArea = ""
	var host = mongodbConfig.Hostname
	var hostPart = host
	var port = mongodbConfig.Port
	if len(port) > 0 {
		hostPart = fmt.Sprintf("%s:%s", host, port)
	}
	var username = mongodbConfig.Username
	var password = mongodbConfig.Password
	if len(username) > 0 || len(password) > 0 {
		authArea = fmt.Sprintf("%s:%s@", username, password)
	}
	var dbName = mongodbConfig.DatabaseName
	var dbPart = ""
	if len(dbName) > 0 {
		dbPart = fmt.Sprintf("/%s", dbName)
	}
	var optionsVals = mongodbConfig.Options
	var optionsPart = ""
	if len(optionsVals) > 0 {
		optionsPart = fmt.Sprintf("?%s", optionsVals)
	}
	dsn := fmt.Sprintf("mongodb://%s%s%s%s", authArea, hostPart, dbPart, optionsPart)
	return dsn
}

func (manager *DbManager) getDatabase() MongoClientI {
	return &databaseInstance
}
